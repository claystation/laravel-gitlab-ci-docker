FROM  php:7.4-alpine

COPY ./composer.sh /tmp/

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV PATH ./vendor/bin:/root/.composer/vendor/bin:$PATH

RUN chmod +x /tmp/*.sh \
    && sh /tmp/composer.sh \
    && mv composer.phar /usr/local/bin/composer
RUN composer global require "squizlabs/php_codesniffer=*"

RUN apk update \
    && apk upgrade \
    && apk add --no-cache git \
    openssh-client \
    nodejs \
    nodejs-npm \
    rsync \
    freetype-dev \
    libjpeg-turbo-dev \
    libpng-dev \
    libzip-dev \
    && apk add --no-cache --virtual .build-deps \
    build-base \
    autoconf \
    && rm -rf /usr/share/man

RUN docker-php-ext-install gd \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip

RUN pecl install xdebug \
    && docker-php-ext-enable xdebug

RUN apk del -f .build-deps

CMD ["php", "-a"]
