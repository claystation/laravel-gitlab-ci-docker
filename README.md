# laravel-gitlab-ci-docker

[![pipeline status](https://gitlab.com/claystation/laravel-gitlab-ci-docker/badges/master/pipeline.svg)](https://gitlab.com/claystation/laravel-gitlab-ci-docker/commits/master)

This is a docker image for running Laravel Tests with phpunit in Gitlab CI. [Docker Hub](https://hub.docker.com/r/claystation/laravel-gitlab-ci-docker/)
